
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#242424" "#e5786d" "#95e454" "#cae682" "#8ac6f2" "#333366" "#ccaa8f" "#f6f3e8"])
 '(current-language-environment "UTF-8"))

;;set font
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(set-default-font "-*-Menlo-normal-normal-normal-*-18-*-*-*-m-0-iso10646-1")

;; line number settings
(global-linum-mode 1)
(global-hl-line-mode 1)

;;set theme
(add-to-list 'load-path "~/.emacs.d/plugins/color-theme-6.6.0")
(require 'color-theme)
(color-theme-initialize)
(color-theme-xp)

;; 隐藏工具栏、菜单栏
(tool-bar-mode 0)
(menu-bar-mode 0)

;;不显示开始画面
(setq inhibit-startup-message 1)

;; 启动最大化
;;(defun fullscreen (&optional f)
;;  (interactive)
;;  (x-send-client-message nil 0 nil "_NET_WM_STATE" 32
;;			 '(2 "_NET_WM_STATE_MAXIMIZED_VERT" 0))
;;  (x-send-client-message nil 0 nil "_NET_WM_STATE" 32
;;			 '(2 "_NET_WM_STATE_MAXIMIZED_HORZ" 0)))
;;(add-hook 'window-setup-hook 'fullscreen)

;; 自动识别编码
(add-to-list 'load-path "~/.emacs.d/plugins")
(require 'unicad)
